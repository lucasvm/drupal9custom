<?php

namespace Drupal\kadabrait_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\kadabrait_content\GetList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\block\Entity\Block;

/**
 * Controller for the list message.
 */
class KadabraitController extends ControllerBase {

  /**
   * The list service.
   *
   * @var \Drupal\kadabrait_content\GetList
   */
  protected $getlist;

  /**
   * KadrabraitController constructor.
   *
   * @param \Drupal\kadabrait_content\GetList $getlist
   *   The getlist service.
   */
  public function __construct(GetList $getlist) {
    $this->getlist = $getlist;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('kadabrait_content.getlist')
    );
  }

  /**
   * Get node list from service.
   *
   * @return array
   *   Our message.
   */
  public function list() {

     $getNodeList = $this->getlist->getListComponent();

     //Assing region to custom block
     $block = Block::load('articlecustomblock');
     $block->setRegion('sidebar_first');
     $block->save();

      return [
        '#theme'  => 'kadabrait_content',
        '#title' => $getNodeList,
      ];

  }

}